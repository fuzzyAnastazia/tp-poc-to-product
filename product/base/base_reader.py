class BaseReader(object):
    def __init__(self, input_path):
        self.input_path = input_path

    def read_data(self):
        raise NotImplementedError
