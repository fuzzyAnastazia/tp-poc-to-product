import logging
import os
from utils.io.data_reader import DataReader


def main():

    logging.basicConfig(level=logging.INFO, format='%(relativeCreated)6d %(threadName)s %(message)s')

    print(os.environ)

    dataReader = DataReader(os.environ['INPUT_PATH'])
    print(dataReader.read_data())


if __name__ == '__main__':
    main()
