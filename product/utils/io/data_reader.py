import pandas as pd
from base.base_reader import BaseReader

class DataReader(BaseReader):
    def __init__(self, input_path):
        super().__init__(
            input_path)
        self.input_path = input_path


    def read_data(self):
        """
        Read csv to dataframe
        :return: dataframe with raw not processed data
        """
        raw_df = pd.read_csv(self.input_path)
        return raw_df