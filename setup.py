#!/usr/bin/env python
# -*- encoding: utf-8 -*-
from __future__ import absolute_import
from __future__ import print_function

import io
from os.path import dirname
from os.path import join


from setuptools import find_packages
from setuptools import setup


def read(*names, **kwargs):
    with io.open(
        join(dirname(__file__), *names),
        encoding=kwargs.get('encoding', 'utf8')
    ) as fh:
        return fh.read()


setup(
    name='tp-product-learner',
    version='0.1.0',
    author='',
    packages=find_packages('product'),
    package_dir={'': 'product'},
    include_package_data=True,
    zip_safe=False,
    tests_require=[
        'pytest'
    ],
    entry_points={
        'console_scripts': [
            'product_go=product.main:main',
        ],
    },
)