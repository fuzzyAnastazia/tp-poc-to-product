FROM python:3.6-buster

USER root
RUN apt-get update
copy . /run
WORKDIR /run

RUN pip3 install --upgrade pip
RUN pip3 install --upgrade setuptools

RUN pwd
RUN pip3 install --no-cache-dir -r /run/requirements.txt

RUN echo complete

CMD ["python3","product/main.py"]
