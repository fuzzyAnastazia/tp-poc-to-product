﻿## Project Instructions

1. Transform the jupyter notebook into industrializable python project given this template. The goal of MVP is to have two processes : 1) train model to learn global consumption once (and possible retrain if needed)
 , save it. 2) use saved model to make weekly prediction : based on the latest two weeks available predict the global consumption of the next week 
2. Create the backlog of tasks to realize and provide the link into this Readme : it can be trello, excel or gitlab issues
3. Work in team, choosing your git workflow where every team member contributes by creating feature branches, merge requests and making merge request reviews 
4. The template of project is optional, and if you want to propose better structuring of python projet, dockerization or scripting CI/CD pipeline, 
you are welcome as long as it hepls to avoid anti-patterns and minimize the technical debt
5. Make sure that the project is runnable with docker (you can test it with docker-compose) and all unit tests pass
6. Bonus : You can modify CI pipeline adding static code analysis with pylint 

## What this project does


## how to run locally

install virtualenv

```python3 -m pip install --user virtualenv```

create a virtualenv for laucning this project

```python3 -m virtualenv pocenv```

activate it

```source pocenv/bin/activate```

go to the project folder and launch

```
    cd tp-product-learner

    pip3 install --no-cache-dir -r requirements.txt

    python product/main.py 
   ```

## How to run with docker

do not forget update docker-compose.yml parameters 

```
   docker build -t epf/tp-product-learner:latest .

   cd stack/ 

   docker-compose up 
   ```


## how to run test

```
   python setup.py clean --all

   python setup.py install

  python -m pytest -vv
  ```


## How to launch

## How to conribute

## To do 